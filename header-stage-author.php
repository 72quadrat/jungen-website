<?php
/**
 * special stage content for author pages
 * here is the author bio and a video displayed
 * if there is a video in the jabber part of
 * the author profile in wordpress
 *
 * @package jungen
 */
?>
<div class="container">
	<div id="stage">
		<div class="author">
			<?php
				/* Queue the first post, that way we know
				 * what author we're dealing with (if that is the case).
				*/
				the_post();
				/* Since we called the_post() above, we need to
				 * rewind the loop back to the beginning that way
				 * we can run the loop properly, in full.
				 */
				rewind_posts();
			?>
			<?php if(get_the_author_meta('id')) : ?>
				<div class="avatar-wrap"><?php echo get_avatar( get_the_author_meta('id'), 120 ); ?></div>
				<article>
					<h1 class="page-title">
						<?php 
						if(get_the_author_meta('display_name')){
							the_author_meta('display_name'); 
						} else {
							echo 'Fehler: Kein Spitzname angegeben';
						}
						?>
					</h1>
					<?php 
					if(get_the_author_meta('description')) {
						echo '<p>';
						the_author_meta('description');
						echo '</p>';
					}
					?>
					<?php 
						/* 	If We have a Link in the Jabber Info
						* 	of the user use this as a youtube video
						*/
						if (strlen(get_the_author_meta( 'jabber' )) > 8) {
							echo '<p>'.wp_oembed_get( get_the_author_meta( 'jabber' ) ).'</p>';
						}
					?>
					<?php
						/* 	If there is a "Redaktionsgruppe" given and we can find that
						* 	print a link to the gropup
						*/

						// Get the given Data from profile page
						$group_given = trim(get_the_author_meta('yim'));

						// Search for User by Username
						if (username_exists($group_given)) {
							$group_author = get_userdata(username_exists($group_given));
							$found = TRUE;
						// Search for User by ID
						} else if (get_userdata($group_given)) {
							$group_author = get_userdata($group_given);
							$found = TRUE;
						} else {
							$found = FALSE;							
						}
						$html = '<p>';
						$html .= '<a href="'.get_author_posts_url($group_author->data->ID).'" class="btn btn-primary">Zur Redaktionsgruppe "'.$group_author->data->user_nicename.'"</a>';
						$html .= '</p>';

						// Output HTML
						if ($found) { echo $html; }

					?>
				</article>
			<?php endif ?>
		</div><!-- /.author -->
	</div><!-- /#stage -->
</div>