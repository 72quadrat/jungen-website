<?php
/*
Template Name: Media Gallery
*/

get_header(); ?>

	<div id="primary" class="content-area span9">
		<div id="content" class="site-content" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'page' ); ?>

			<?php endwhile; // end of the loop. ?>

			<?php $query_images_args = array(
				'post_type' => 'attachment', 
				'post_mime_type' =>'image', 
				'post_status' => 'published', 
				'posts_per_page' => -1,
			);

			$query_images = new WP_Query( $query_images_args );
			$images = array();
			foreach ( $query_images->posts as $image) {
				if(get_post_type($image->post_parent) == 'post' ){
					$images[]= array(
						'ID' => $image->ID,
						'URL_thumb' => wp_get_attachment_thumb_url( $image->ID ),
						'HTMLTAG' => wp_get_attachment_image($image->ID, 'medium'),
						'post_title' => $image->post_title,
						'post_name' => $image->post_name,
						'post_content' => $image->post_content,
						'post_parent' => $image->post_parent,
						'post_parent_url' => get_permalink($image->post_parent),
						'post_parent_title' => get_the_title($image->post_parent),
					);

				}

			}?>

			<ul class="thumbnails" id="pinterest" style="position: relative;">
				<?php foreach ($images as $key => $image) : ?>
					<li class="span3 pinterest">
						<?php echo $image['HTMLTAG']; ?>
						<!--<a href="<?php echo $image['post_parent_url']?>" class="thumbnail"><img src="<?php echo $image['URL_thumb']?>" alt="<?php echo $image['post_title']?> <?php echo $image['post_name']?>"></a>-->
						<h4><?php echo $image['post_title']?></h4>
						<p>verwendet in Beitrag "<?php echo $image['post_parent_title']?>"</p>
						<p><a href="<?php echo $image['post_parent_url']?>" class="btn">Zum Beitrag</a></p>
					</li>
				<?php endforeach ?>
			</ul>

			<script>

				/*! jQuery wookmark plugin */
				(function(t){"function"==typeof define&&define.amd?define(["jquery"],t):t(jQuery)})(function(t){var e,s,h;h=function(t,i){return function(){return t.apply(i,arguments)}},s={align:"center",container:t("body"),offset:2,autoResize:!1,itemWidth:0,flexibleWidth:0,resizeDelay:50,onLayoutChanged:void 0,fillEmptySpace:!1},e=function(){function e(i,e){this.handler=i,this.columns=this.containerWidth=this.resizeTimer=null,this.activeItemCount=0,this.direction="left",this.itemHeightsDirty=!0,this.placeholders=[],t.extend(!0,this,s,e),this.update=h(this.update,this),this.onResize=h(this.onResize,this),this.onRefresh=h(this.onRefresh,this),this.getItemWidth=h(this.getItemWidth,this),this.layout=h(this.layout,this),this.layoutFull=h(this.layoutFull,this),this.layoutColumns=h(this.layoutColumns,this),this.filter=h(this.filter,this),this.clear=h(this.clear,this),this.getActiveItems=h(this.getActiveItems,this),this.refreshPlaceholders=h(this.refreshPlaceholders,this);for(var o,n=j=0,r={};i.length>n;n++)if($item=i.eq(n),o=$item.data("filterClass"),"object"==typeof o&&o.length>0)for(j=0;o.length>j;j++)filterClass=t.trim(o[j]).toLowerCase(),filterClass in r||(r[filterClass]=[]),r[filterClass].push($item[0]);this.filterClasses=r,this.autoResize&&t(window).bind("resize.wookmark",this.onResize),this.container.bind("refreshWookmark",this.onRefresh)}return e.prototype.update=function(i){this.itemHeightsDirty=!0,t.extend(!0,this,i)},e.prototype.onResize=function(){clearTimeout(this.resizeTimer),this.itemHeightsDirty=0!=this.flexibleWidth,this.resizeTimer=setTimeout(this.layout,this.resizeDelay)},e.prototype.onRefresh=function(){this.itemHeightsDirty=!0,this.layout()},e.prototype.filter=function(i,e){var s,h,o,n,r,a=[],l=t();if(i=i||[],e=e||"or",i.length){for(h=0;i.length>h;h++)r=t.trim(i[h].toLowerCase()),r in this.filterClasses&&a.push(this.filterClasses[r]);if(s=a.length,"or"==e||1==s)for(h=0;s>h;h++)l=l.add(a[h]);else if("and"==e){var f,u,c,d=a[0],m=!0;for(h=1;s>h;h++)a[h].length<d.length&&(d=a[h]);for(h=0;d.length>h;h++){for(u=d[h],m=!0,o=0;a.length>o&&m;o++)if(c=a[o],d!=c){for(n=0,f=!1;c.length>n&&!f;n++)f=c[n]==u;m&=f}m&&l.push(d[h])}}this.handler.not(l).addClass("inactive")}else l=this.handler;l.removeClass("inactive"),this.columns=null,this.layout()},e.prototype.refreshPlaceholders=function(i,e){for(var s,h,o,n,r,a=this.placeholders.length,l=this.columns.length,f=this.container.outerHeight();l>a;a++)s=t('<div class="wookmark-placeholder"/>').appendTo(this.container),this.placeholders.push(s);for(innerOffset=this.offset+2*parseInt(this.placeholders[0].css("borderWidth")),a=0;this.placeholders.length>a;a++)if(s=this.placeholders[a],o=this.columns[a],a>=l||!o[o.length-1])s.css("display","none");else{if(h=o[o.length-1],!h)continue;r=h.data("wookmark-top")+h.data("wookmark-height")+this.offset,n=f-r-innerOffset,s.css({position:"absolute",display:n>0?"block":"none",left:a*i+e,top:r,width:i-innerOffset,height:n})}},e.prototype.getActiveItems=function(){return this.handler.not(".inactive")},e.prototype.getItemWidth=function(){var t=this.itemWidth,i=this.container.width(),e=this.handler.eq(0),s=this.flexibleWidth;if(void 0===this.itemWidth||0===this.itemWidth&&!this.flexibleWidth?t=e.outerWidth():"string"==typeof this.itemWidth&&this.itemWidth.indexOf("%")>=0&&(t=parseFloat(this.itemWidth)/100*i),s){"string"==typeof s&&s.indexOf("%")>=0&&(s=parseFloat(s)/100*i-e.outerWidth()+e.innerWidth());var h=~~(1+i/(s+this.offset)),o=(i-(h-1)*this.offset)/h;t=Math.max(t,~~o),this.handler.css("width",t)}return t},e.prototype.layout=function(){if(this.container.is(":visible")){var t,e=this.getItemWidth()+this.offset,s=this.container.width(),h=~~((s+this.offset)/e),o=maxHeight=i=0,n=this.getActiveItems(),r=n.length;if(this.itemHeightsDirty){for(;r>i;i++)t=n.eq(i),t.data("wookmark-height",t.outerHeight());this.itemHeightsDirty=!1}h=Math.max(1,Math.min(h,r)),o="left"==this.align||"right"==this.align?~~(h/e+this.offset>>1):~~(.5+(s-(h*e-this.offset))>>1),this.direction="right"==this.align?"right":"left",maxHeight=null!=this.columns&&this.columns.length==h&&this.activeItemCount==r?this.layoutColumns(e,o):this.layoutFull(e,h,o),this.activeItemCount=r,this.container.css("height",maxHeight),this.fillEmptySpace&&this.refreshPlaceholders(e,o),void 0!==this.onLayoutChanged&&"function"==typeof this.onLayoutChanged&&this.onLayoutChanged()}},e.prototype.layoutFull=function(t,i,e){var s,h=0,o=0,n=this.getActiveItems(),r=n.length,a=null,l=null,f={position:"absolute"},u=[],c="left"==this.align?!0:!1;for(this.columns=[];i>u.length;)u.push(0),this.columns.push([]);for(;r>h;h++){for($item=n.eq(h),a=u[0],l=0,o=0;i>o;o++)a>u[o]&&(a=u[o],l=o);s=0==l&&c?0:l*t+e,f[this.direction]=s,f.top=a,$item.css(f).data("wookmark-top",a),u[l]+=$item.data("wookmark-height")+this.offset,this.columns[l].push($item)}return Math.max.apply(Math,u)},e.prototype.layoutColumns=function(t,i){for(var e,s,h,o=[],n=0,r=0;this.columns.length>n;n++){for(o.push(0),e=this.columns[n],h=n*t+i,currentHeight=o[n],r=0;e.length>r;r++)$item=e[r],s={top:currentHeight},s[this.direction]=h,$item.css(s).data("wookmark-top",currentHeight),currentHeight+=$item.data("wookmark-height")+this.offset;o[n]=currentHeight}return Math.max.apply(Math,o)},e.prototype.clear=function(){clearTimeout(this.resizeTimer),t(window).unbind("resize.wookmark",this.onResize),this.container.unbind("refreshWookmark",this.onRefresh)},e}(),t.fn.wookmark=function(t){return this.wookmarkInstance?this.wookmarkInstance.update(t||{}):this.wookmarkInstance=new e(this,t||{}),this.wookmarkInstance.layout(),this.show()}});

				/*!
 				* imagesLoaded PACKAGED v3.0.2
 				* JavaScript is all like "You images are done yet or what?"
 				*/
				(function(e){"use strict";function t(){}function n(e,t){if(r)return t.indexOf(e);for(var n=t.length;n--;)if(t[n]===e)return n;return-1}var i=t.prototype,r=Array.prototype.indexOf?!0:!1;i._getEvents=function(){return this._events||(this._events={})},i.getListeners=function(e){var t,n,i=this._getEvents();if("object"==typeof e){t={};for(n in i)i.hasOwnProperty(n)&&e.test(n)&&(t[n]=i[n])}else t=i[e]||(i[e]=[]);return t},i.getListenersAsObject=function(e){var t,n=this.getListeners(e);return n instanceof Array&&(t={},t[e]=n),t||n},i.addListener=function(e,t){var i,r=this.getListenersAsObject(e);for(i in r)r.hasOwnProperty(i)&&-1===n(t,r[i])&&r[i].push(t);return this},i.on=i.addListener,i.defineEvent=function(e){return this.getListeners(e),this},i.defineEvents=function(e){for(var t=0;e.length>t;t+=1)this.defineEvent(e[t]);return this},i.removeListener=function(e,t){var i,r,s=this.getListenersAsObject(e);for(r in s)s.hasOwnProperty(r)&&(i=n(t,s[r]),-1!==i&&s[r].splice(i,1));return this},i.off=i.removeListener,i.addListeners=function(e,t){return this.manipulateListeners(!1,e,t)},i.removeListeners=function(e,t){return this.manipulateListeners(!0,e,t)},i.manipulateListeners=function(e,t,n){var i,r,s=e?this.removeListener:this.addListener,o=e?this.removeListeners:this.addListeners;if("object"!=typeof t||t instanceof RegExp)for(i=n.length;i--;)s.call(this,t,n[i]);else for(i in t)t.hasOwnProperty(i)&&(r=t[i])&&("function"==typeof r?s.call(this,i,r):o.call(this,i,r));return this},i.removeEvent=function(e){var t,n=typeof e,i=this._getEvents();if("string"===n)delete i[e];else if("object"===n)for(t in i)i.hasOwnProperty(t)&&e.test(t)&&delete i[t];else delete this._events;return this},i.emitEvent=function(e,t){var n,i,r,s=this.getListenersAsObject(e);for(i in s)if(s.hasOwnProperty(i))for(n=s[i].length;n--;)r=t?s[i][n].apply(null,t):s[i][n](),r===!0&&this.removeListener(e,s[i][n]);return this},i.trigger=i.emitEvent,i.emit=function(e){var t=Array.prototype.slice.call(arguments,1);return this.emitEvent(e,t)},"function"==typeof define&&define.amd?define(function(){return t}):e.EventEmitter=t})(this),function(e){"use strict";var t=document.documentElement,n=function(){};t.addEventListener?n=function(e,t,n){e.addEventListener(t,n,!1)}:t.attachEvent&&(n=function(t,n,i){t[n+i]=i.handleEvent?function(){var t=e.event;t.target=t.target||t.srcElement,i.handleEvent.call(i,t)}:function(){var n=e.event;n.target=n.target||n.srcElement,i.call(t,n)},t.attachEvent("on"+n,t[n+i])});var i=function(){};t.removeEventListener?i=function(e,t,n){e.removeEventListener(t,n,!1)}:t.detachEvent&&(i=function(e,t,n){e.detachEvent("on"+t,e[t+n]);try{delete e[t+n]}catch(i){e[t+n]=void 0}});var r={bind:n,unbind:i};"function"==typeof define&&define.amd?define(r):e.eventie=r}(this),function(e){"use strict";function t(e,t){for(var n in t)e[n]=t[n];return e}function n(e){return"[object Array]"===a.call(e)}function i(e){var t=[];if(n(e))t=e;else if("number"==typeof e.length)for(var i=0,r=e.length;r>i;i++)t.push(e[i]);else t.push(e);return t}function r(e,n){function r(e,n,o){if(!(this instanceof r))return new r(e,n);"string"==typeof e&&(e=document.querySelectorAll(e)),this.elements=i(e),this.options=t({},this.options),"function"==typeof n?o=n:t(this.options,n),o&&this.on("always",o),this.getImages(),s&&(this.jqDeferred=new s.Deferred);var h=this;setTimeout(function(){h.check()})}function a(e){this.img=e}r.prototype=new e,r.prototype.options={},r.prototype.getImages=function(){this.images=[];for(var e=0,t=this.elements.length;t>e;e++){var n=this.elements[e];"IMG"===n.nodeName&&this.addImage(n);for(var i=n.querySelectorAll("img"),r=0,s=i.length;s>r;r++){var o=i[r];this.addImage(o)}}},r.prototype.addImage=function(e){var t=new a(e);this.images.push(t)},r.prototype.check=function(){function e(e,r){return t.options.debug&&h&&o.log("confirm",e,r),t.progress(e),n++,n===i&&t.complete(),!0}var t=this,n=0,i=this.images.length;if(this.hasAnyBroken=!1,!i)return this.complete(),void 0;for(var r=0;i>r;r++){var s=this.images[r];s.on("confirm",e),s.check()}},r.prototype.progress=function(e){this.hasAnyBroken=this.hasAnyBroken||!e.isLoaded,this.emit("progress",this,e),this.jqDeferred&&this.jqDeferred.notify(this,e)},r.prototype.complete=function(){var e=this.hasAnyBroken?"fail":"done";if(this.isComplete=!0,this.emit(e,this),this.emit("always",this),this.jqDeferred){var t=this.hasAnyBroken?"reject":"resolve";this.jqDeferred[t](this)}},s&&(s.fn.imagesLoaded=function(e,t){var n=new r(this,e,t);return n.jqDeferred.promise(s(this))});var f={};return a.prototype=new e,a.prototype.check=function(){var e=f[this.img.src];if(e)return this.useCached(e),void 0;if(f[this.img.src]=this,this.img.complete&&void 0!==this.img.naturalWidth)return this.confirm(0!==this.img.naturalWidth,"naturalWidth"),void 0;var t=this.proxyImage=new Image;n.bind(t,"load",this),n.bind(t,"error",this),t.src=this.img.src},a.prototype.useCached=function(e){if(e.isConfirmed)this.confirm(e.isLoaded,"cached was confirmed");else{var t=this;e.on("confirm",function(e){return t.confirm(e.isLoaded,"cache emitted confirmed"),!0})}},a.prototype.confirm=function(e,t){this.isConfirmed=!0,this.isLoaded=e,this.emit("confirm",this,t)},a.prototype.handleEvent=function(e){var t="on"+e.type;this[t]&&this[t](e)},a.prototype.onload=function(){this.confirm(!0,"onload"),this.unbindProxyEvents()},a.prototype.onerror=function(){this.confirm(!1,"onerror"),this.unbindProxyEvents()},a.prototype.unbindProxyEvents=function(){n.unbind(this.proxyImage,"load",this),n.unbind(this.proxyImage,"error",this)},r}var s=e.jQuery,o=e.console,h=o!==void 0,a=Object.prototype.toString;"function"==typeof define&&define.amd?define(["eventEmitter","eventie"],r):e.imagesLoaded=r(e.EventEmitter,e.eventie)}(window);
				jQuery(document).ready(function() {
					jQuery('#pinterest').imagesLoaded(function(){
						jQuery('#pinterest .pinterest').wookmark({
							align: 'center',
							autoResize: true,
							container: jQuery('#pinterest'),
							itemWidth: 0,
							offset: 20,
							resizeDelay: 50,
							flexibleWidth: 0,
							onLayoutChanged: undefined,
							fillEmptySpace: false						
						});
					})
				});

			</script>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
