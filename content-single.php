<?php
/**
 * Template for single entry
 * 
 * @package jungen
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<h1 class="entry-title bigtext"><?php the_title(); ?></h1>

		<div class="entry-meta">
			<ul class="inline">
				<?php jungen_posted_on(); ?>
			</ul>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

	<div class="row">
		<div class="avatar-wrap span2">
			<?php $user_id = get_the_author_meta('id');?>
			<a href="<?php echo get_author_posts_url( $user_id ); ?>" data-toggle="tooltip" title="Autor: <?php the_author(); ?>">
				<?php echo get_avatar( $user_id, 170 ); ?>
			</a>
		</div><!-- /span2 -->
		<div class="span7">
			<?php 
				if ( has_post_thumbnail() AND !has_post_format('video') ) { // check if the post has a Post Thumbnail assigned to it.

					echo '<div class="feature-thumb">';
					the_post_thumbnail('feature-thumb');
					echo '</div>';
				} 
			?>
			<div class="entry-content">
				<?php the_content(); ?>
				<?php
					wp_link_pages( array(
						'before' => '<div class="page-links">' . __( 'Pages:', 'jungen' ),
						'after'  => '</div>',
					) );
				?>
			</div><!-- .entry-content -->

			<footer class="entry-meta well">
				<?php
					/* translators: used between list items, there is a space after the comma */
					$category_list = get_the_category_list( __( ', ', 'jungen' ) );

					/* translators: used between list items, there is a space after the comma */
					$tag_list = get_the_tag_list( '', __( ', ', 'jungen' ) );

					if ( ! jungen_categorized_blog() ) {
						// This blog only has 1 category so we just need to worry about tags in the meta text
						if ( '' != $tag_list ) {
							$meta_text = __( 'This entry was tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'jungen' );
						} else {
							$meta_text = __( 'Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'jungen' );
						}

					} else {
						// But this blog has loads of categories so we should probably display them here
						if ( '' != $tag_list ) {
							$meta_text = __( 'This entry was posted in %1$s and tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'jungen' );
						} else {
							$meta_text = __( 'This entry was posted in %1$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'jungen' );
						}

					} // end check for categories on this blog

					printf(
						$meta_text,
						$category_list,
						$tag_list,
						get_permalink(),
						the_title_attribute( 'echo=0' )
					);

				?>

				<?php if(get_field('bildnachweis')){echo '<p>' . get_field('bildnachweis') . '</p>';}?>

				<?php edit_post_link( __( 'Edit', 'jungen' ), '<span class="edit-link">', '</span>' ); ?>
			</footer><!-- .entry-meta -->
		</div><!-- /span7 -->
		<?php if (function_exists('related_posts')) { related_posts(); } ?>
	</div><!-- /row -->
</article><!-- #post-## -->
