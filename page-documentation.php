<?php
/*
Template Name: Dokumentation
*/

get_header(); ?>

	<div id="primary" class="content-area span9">
		<div id="content" class="site-content" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'page' ); ?>

			<?php endwhile; // end of the loop. ?>

		</div><!-- #content -->
	</div><!-- #primary -->

	<div id="secondary" class="widget-area span3" role="complementary">
		<aside id="nav_menu-2" class="widget well">
			<!--<h1 class="widget-title">Seiten</h1>-->
			<div class="menu-sidebar-menu-container">
				<ul class="nav nav-list">
					<li><a href="/dokumentation"><i class="icon-home"></i>Doku-Startseite</a></li>
					<li class="divider"></li>
					<?php $id = get_the_id() ?>
					<?php wp_list_pages('child_of=1985&private&title_li=&exclude='.$id.'&link_before=<i class="icon-chevron-right"></i>'); ?> 
				</ul>
			</div>
		</aside>
	</div>

<?php get_footer(); ?>
<style>
	
</style>