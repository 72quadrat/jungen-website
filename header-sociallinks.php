<?php
/**
 * The Social Links with hand coded links – currently to nowhere
 * The Icons are just images
 *
 * @package jungen
 */
?>
<div class="sociallinks">
	<ul class="inline">
		<li><a href="http://wiki.meintestgelaende.de" data-toggle="tooltip" title="Das Wiki von Mein Testgelände" target="_blank"><img src="<?php bloginfo('template_directory')?>/img/btn-wiki.png" alt="Facebook"/></a><span class="visuallyhidden">Das Wiki von Mein Testgelände</span></li>
		<li><a href="https://www.facebook.com/meintestgelaende" data-toggle="tooltip" title="Mein Testgelände bei Facebook" target="_blank"><img src="<?php bloginfo('template_directory')?>/img/btn-facebook.png" alt="Facebook"/></a><span class="visuallyhidden">Mein Testgelände auf Facebook</span></li>
		<li><a href="https://twitter.com/Testgelaende" data-toggle="tooltip" title="Mein Testgelände bei Twitter" target="_blank"><img src="<?php bloginfo('template_directory')?>/img/btn-twitter.png" alt="Twitter"/><span class="visuallyhidden">Mein Testgelände bei Twitter</span></a></li>
		<li><a href="https://www.youtube.com/channel/UCKMzrrhQ5AiNzYm-Sqv9qZA" data-toggle="tooltip" title="Mein Testgelände bei Youtube" target="_blank"><img src="<?php bloginfo('template_directory')?>/img/btn-youtube.png" alt="Youtube"/><span class="visuallyhidden">Mein Testgelände auf Youtube</span></a></li>
	</ul>
</div>