<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features
 *
 * @package jungen
 */


if ( ! function_exists( 'jungen_content_nav' ) ) :
/**
 * Display navigation to next/previous pages when applicable
 */
function jungen_content_nav( $nav_id ) {
	global $wp_query, $post;

	// Don't print empty markup on single pages if there's nowhere to navigate.
	if ( is_single() ) {
		$previous = ( is_attachment() ) ? get_post( $post->post_parent ) : get_adjacent_post( false, '', true );
		$next = get_adjacent_post( false, '', false );

		if ( ! $next && ! $previous )
			return;
	}

	// Don't print empty markup in archives if there's only one page.
	if ( $wp_query->max_num_pages < 2 && ( is_home() || is_archive() || is_search() ) )
		return;

	$nav_class = ( is_single() ) ? 'navigation-post' : 'navigation-paging';

	?>
	<nav role="navigation" id="<?php echo esc_attr( $nav_id ); ?>" class="<?php echo $nav_class; ?> row">
		<div class="span7 offset3">
			<h3 class="screen-reader-text"><?php _e( 'Post navigation', 'jungen' ); ?></h3>

			<?php if ( is_single() ) : // navigation links for single posts ?>

				<?php previous_post_link( '<div class="nav-previous">%link</div>', '<span class="meta-nav">' . _x( '&larr;', 'Previous post link', 'jungen' ) . '</span> %title' ); ?>
				<?php next_post_link( '<div class="nav-next">%link</div>', '%title <span class="meta-nav">' . _x( '&rarr;', 'Next post link', 'jungen' ) . '</span>' ); ?>

			<?php elseif ( $wp_query->max_num_pages > 1 && ( is_home() || is_archive() || is_search() ) ) : // navigation links for home, archive, and search pages ?>

				<div class="pagination">
					<ul>
						<?php
						global $wp_query;
						$big = 999999999; // need an unlikely integer
						echo paginate_links( array(
							'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
							'format' => '?paged=%#%',
							'current' => max( 1, get_query_var('paged') ),
							'type' => 'list',
							'total' => $wp_query->max_num_pages
						) );
						?>
					</ul>
				</div>

			<?php endif; ?>
		</div>

	</nav><!-- #<?php echo esc_html( $nav_id ); ?> -->
	<?php
}
endif; // jungen_content_nav

if ( ! function_exists( 'jungen_comment' ) ) :
/**
 * Template for comments and pingbacks.
 *
 * Used as a callback by wp_list_comments() for displaying the comments.
 */
function jungen_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
	?>
	<li class="post pingback">
		<p><?php _e( 'Pingback:', 'jungen' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __( 'Edit', 'jungen' ), '<span class="edit-link">', '<span>' ); ?></p>
	<?php
			break;
		default :
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<div class="comment-meta commentmetadata">
			<a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>"><time datetime="<?php comment_time( 'c' ); ?>">
			<?php printf( _x( '%1$s um %2$s Uhr', '1: date, 2: time', 'jungen' ), get_comment_date(), get_comment_time() ); ?>
			</time></a>
			<?php edit_comment_link( __( 'Edit', 'jungen' ), '<span class="edit-link">', '<span>' ); ?>
		</div><!-- .comment-meta .commentmetadata -->
		<?php echo get_avatar( $comment, 60 ); ?>
		<article id="comment-<?php comment_ID(); ?>" class="comment">
			<footer>
				<div class="comment-author vcard">
					<?php printf( __( '%s <span class="says">says:</span>', 'jungen' ), sprintf( '<cite class="fn">%s</cite>', get_comment_author_link() ) ); ?>
				</div><!-- .comment-author .vcard -->
				<?php if ( $comment->comment_approved == '0' ) : ?>
					<em><?php _e( 'Your comment is awaiting moderation.', 'jungen' ); ?></em>
					<br />
				<?php endif; ?>
			</footer>

			<div class="comment-content"><?php comment_text(); ?></div>

			<div class="reply">
			<?php
				comment_reply_link( array_merge( $args,array(
					'depth'     => $depth,
					'max_depth' => $args['max_depth'],
				) ) );
			?>
			</div><!-- .reply -->
		</article><!-- #comment-## -->

	<?php
			break;
	endswitch;
}
endif; // ends check for jungen_comment()

if ( ! function_exists( 'jungen_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function jungen_posted_on() {


	// Get Variables to check
	$tags_list = get_the_tag_list('',', ','');
	$commentstext = get_comments_number().' Kommentare';
	if(get_comments_number() == 1){
		$commentstext = '1 Kommentar';
	};

	// Create HTML
	$html='<!-- posted on -->';
	if ( 'post' == get_post_type() ){
		$html.='<li><i class="icon-time"></i> '.get_the_time('j. F Y').'</li>';
	}
	// Create Author List with Co-Authors-Plus plugin or standard kind of
	if ( function_exists( 'coauthors_posts_links' ) ) {
		$authorstring = coauthors_posts_links( ', ',', ','','',false );
		// remove space before commata
		$authorstring = str_replace(' , ',', ',$authorstring);
	} else {
		$authorstring = '<a href="'.get_author_posts_url(get_the_author_meta('ID')).'" title="Alle Beiträge dieses Autors">'.get_the_author_meta('nicename').'</a>';
	}
	$html.='<li><i class="icon-user"></i> '.$authorstring.'</li>';
	if ( 'post' == get_post_type() ){
		$html.='<li class="cats-list">';
		$html.='	<i class="icon-folder-open"></i> '.get_the_category_list(', ');
		$html.='</li>';
		if( $tags_list ){
			/* Only show Tags list if there are any */
			// $html.='<li class="tags-list">';
			// $html.='	<i class="icon-tags"></i> Tags: '.$tags_list;
			// $html.='</li>';
		}
		if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ){
			$html.='<li class="comments-count">';
			$html.='	<i class="icon-comment-alt"></i> <a href="'.get_permalink().'#comments" title="Zu den Kommentaren">'.$commentstext.'</a>';
			$html.='</li>';
		}
	} 
	echo $html;
}
endif;
/**
 * Returns true if a blog has more than 1 category
 */
function jungen_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'all_the_cool_cats' ) ) ) {
		// Create an array of all the categories that are attached to posts
		$all_the_cool_cats = get_categories( array(
			'hide_empty' => 1,
		) );

		// Count the number of categories that are attached to the posts
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'all_the_cool_cats', $all_the_cool_cats );
	}

	if ( '1' != $all_the_cool_cats ) {
		// This blog has more than 1 category so jungen_categorized_blog should return true
		return true;
	} else {
		// This blog has only 1 category so jungen_categorized_blog should return false
		return false;
	}
}

/**
 * Flush out the transients used in jungen_categorized_blog
 */
function jungen_category_transient_flusher() {
	// Like, beat it. Dig?
	delete_transient( 'all_the_cool_cats' );
}
add_action( 'edit_category', 'jungen_category_transient_flusher' );
add_action( 'save_post', 'jungen_category_transient_flusher' );