<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package jungen
 */
?>

	</div><!-- /row -->
	</div><!-- #main -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info container">
			Mein Testgelaende.de ist eine Seite der <a href="http://www.bag-jungenarbeit.de/" title="Zur Seite der Bundesarbeitsgemeinschaft Jungenarbeit">BAG Jungenarbeit</a> und der <a href="http://www.maedchenpolitik.de/" title="Zur Seite der Bundesarbeitsgemeinschaft Mädchenpolitik">BAG Mädchenpolitik</a>
			<img src="<?php echo get_template_directory_uri(); ?>/img/spacer.gif" id="loaded" alt="placeholder">
			<div class="footer-menu"><?php wp_nav_menu( array('menu' => 'Footer' )); ?></div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>