<?php
/**
 * this displays the teaser on frontpage and archive pages
 * mainly it just includes the content-teaser templates based
 * on post format
 *
 * @package jungen
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php if ( has_post_format( 'video' )) : ?>

			<?php get_template_part( 'content-teaser', 'video' );?>

	<?php else : ?>

			<?php get_template_part( 'content-teaser', 'standard' );?>

	<?php endif ?>

</article><!-- #post-## -->
