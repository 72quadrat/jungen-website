<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package jungen
 */
?><!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<meta name="description" content="Webportal von Jugendlichen für Jugendliche aller Geschlechter. Videos, Musik und Texte von jugendlichen Redaktionsgruppen zu Gleichberechtigung und mehr." />
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<script src="<?php echo get_template_directory_uri(); ?>/js/vendor/modernizr.custombuild.2.6.2.min.js"></script>

<!-- Web Font Loading in Head -->
<script src="//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
<script>
	WebFont.load({
		custom: {
			families: ['jungsfont', 'FontAwesome'],
			urls: ['<?php echo get_template_directory_uri(); ?>/fonts/webfonts.css']
		},
		active: function() {
			slabtext();
		}
  });
</script>

<?php if (is_home() || is_page('kontakt')) {
		// Load Script and Styles for Contact Form 7 only on Home and the Contact Page
		if ( function_exists( 'wpcf7_enqueue_scripts' ) ) {
			wpcf7_enqueue_scripts();
			wpcf7_enqueue_styles();
		}
	}
?>

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id ="metanav">
	<div class="container">
		<?php wp_nav_menu( array('menu' => 'Metanavigation' )); ?>
	</div>
</div>
<div id="page" class="hfeed site">
	<?php do_action( 'before' ); ?>
	<header id="masthead" class="site-header" role="banner">
		<div class="container">
			<!--[if lt IE 8]>
				<br/>
				<div id="warning" class="alert alert-error alert-block">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
  					<h4>ACHTUNG!</h4>
  					Sie benutzen einen veralteten Internet-Browser – bitte <a href="http://browser-update.org/de/update.html"><strong>aktualisieren sie ihren Browser</strong></a>
				</div>
			<![endif]-->

			<div class="site-branding">
				<?php if (is_home()) : ?>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><h1 class="site-title bigtext"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.gif" alt="<?php echo(get_bloginfo( 'name' )); ?>"></h1></a>
				<?php else : ?>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><h1 class="site-title bigtext"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.gif" alt="<?php echo(get_bloginfo( 'name' )); ?>"></h1></a>
				<?php endif ?>
				<!-- <h2 class="site-description"><?php bloginfo( 'description' ); ?></h2> -->
			</div>
			<?php get_template_part( 'header', 'sociallinks' );?>

			<!--<nav id="site-navigation" class="navigation-main" role="navigation">
				<h1 class="menu-toggle"><?php _e( 'Menu', 'jungen' ); ?></h1>
				<div class="screen-reader-text skip-link"><a href="#content" title="<?php esc_attr_e( 'Skip to content', 'jungen' ); ?>"><?php _e( 'Skip to content', 'jungen' ); ?></a></div>

				<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
			</nav>-->
			<!-- #site-navigation -->

		</div><!-- /container -->

		<?php 
		if ( is_home() ) {
			get_template_part( 'header', 'stage-home' );
		} elseif ( is_author() ) {
			get_template_part( 'header', 'stage-author' );
		}
		?>

		<?php get_template_part( 'header', 'mainnav' );?>


	</header><!-- #masthead -->

	<div id="main" class="site-main container">
		<div class="row">