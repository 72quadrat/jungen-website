<?php
/**
 * special stage content for homepage
 *
 * @package jungen
 */
?><div class="container">
	<div id="stage">
		<!-- Stage Nav -->
		<ul class="stage-nav nav nav-tabs" id="tab-links">
			<li class="active"><a href="#stage1" data-toggle="tab">Redaktionen</a></li>
			<li><a href="#stage2" data-toggle="tab">Autor*innen</a></li>
			<li><a href="#stage3" data-toggle="tab">Themen</a></li>
		</ul>
		<!-- Stage Content -->
		<div id="stagecontent" class="tab-content">
			<!-- Redaktionen -->
			<div class="tab-pane active fade in" id="stage1">
				<?php $count = 0 ?>
				<?php $wp_users = jungen_get_users_random('editor'); ?>
				<div class="row">
					<div class="span9">
						<ul class="inline freunde">
							<?php foreach ($wp_users as $key => $wp_user) : ?>
								<?php
									// Get User Info
									$wp_user = $wp_user->data;
									// Get Posts count
									$count_user_posts = count_user_posts($wp_user->ID);
								?>
								<?php if ($count_user_posts > 0) : ?>
									<?php 
										$count++; 
										if ($count > 4) {
											$class = "friend friend-hidden";
										} else {
											$class = "friend";
										}
									?>
									<li class="<?php echo $count?>">
										<?php
										// Get String to show amount of posts
										if ($count_user_posts == 1) {
											$beitraegestring = $count_user_posts.' Beitrag';
										} else {
											$beitraegestring = $count_user_posts.' Beiträge';
										}?>

										<a href="<?php echo get_author_posts_url( $wp_user->ID ); ?>" data-toggle="tooltip" title="<?php echo $beitraegestring?>">
											<?php echo get_avatar( $wp_user->ID, 120 ); ?>
										</a>
										<?php echo($wp_user->user_nicename) ?>
									</li>
								<?php endif ?>
							<?php endforeach; ?>
							<?php if ($count > 4) : ?>
								<li class="circle"><a href="#" class="btn-circle"><img src="<?php echo get_template_directory_uri(); ?>/img/circle-show-all.png" alt=""></a></li>
							<?php endif ?>
						</ul>
						<p class="showless"><a href="#" class="btn btn-primary">Weniger zeigen</a></p>
					</div>
					<div class="span3">
						<p>Mein Testgelaende ist von Jugendlichen für Jugendliche.</p>
						<p>Wer die Beiträge erstellt, siehst Du unter Redaktionen und Autor*innen.</p>
					</div>
				</div>            
			</div>
			<!-- Autoren -->
			<div class="tab-pane fade in" id="stage2">
				<?php $count = 0 ?>
				<?php $wp_users = jungen_get_users_random('author'); ?>
				<div class="row">
					<div class="span9">
						<ul class="inline freunde">
							<?php foreach ($wp_users as $key => $wp_user) : ?>
								<?php
									// Get User Info
									$wp_user = $wp_user->data;
									// Get Posts count
									$count_user_posts = count_user_posts($wp_user->ID);
								?>
								<?php if ($count_user_posts > 0) : ?>
									<?php 
										$count++; 
										if ($count > 4) {
											$class = "friend-hidden";
										} else {
											$class = "friend";
										}
									?>
									<li class="<?php echo $class?>">
										<?php
										// Get String to show amount of posts
										if ($count_user_posts == 1) {
											$beitraegestring = $count_user_posts.' Beitrag';
										} else {
											$beitraegestring = $count_user_posts.' Beiträge';
										}?>

										<a href="<?php echo get_author_posts_url( $wp_user->ID ); ?>" data-toggle="tooltip" title="<?php echo $beitraegestring?>">
											<?php echo get_avatar( $wp_user->ID, 120 ); ?>
										</a>
										<?php echo($wp_user->display_name) ?>
									</li>
								<?php endif ?>
							<?php endforeach; ?>
							<?php if ($count > 4) : ?>
								<li class="circle"><a href="#" class="btn-circle"><img src="<?php echo get_template_directory_uri(); ?>/img/circle-show-all.png" alt=""></a></li>
							<?php endif ?>
						</ul>
						<p class="showless"><a href="#" class="btn btn-primary">Weniger zeigen</a></p>
					</div>
					<div class="span3">
						<p>Mein Testgelaende ist von Jugendlichen für Jugendliche.</p>
						<p>Wer die Beiträge erstellt, siehst Du unter Redaktionen und Autor*innen.</p>
					</div>
				</div>             
			</div>
			<!-- Mitmachen -->
			<div class="tab-pane fade in" id="stage3">
				<div class="tag-cloud">
					<?php wp_tag_cloud(array('smallest'=> 12,'largest'=> 40));?>
				</div>
			</div>
		</div><!-- /#stagecontent -->
	</div><!-- /#stage -->
</div>