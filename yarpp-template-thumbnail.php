<?php
/*
YARPP Template: Thumbnails
Description: Thumbnail-Darstellung
Author: neueshandeln
*/ ?>
	<div class="span9">
		<?php if (have_posts()):?>
		<h3>Das könnte dich auch interessieren</h3>
		<ul class="thumbnails">
			<?php while (have_posts()) : the_post(); ?>
				<li class="span3">
					<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>" class="thumbnail">
						<?php 
							if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
								the_post_thumbnail(array(400,400));
							} else {
								echo '<img src="'.get_template_directory_uri().'/img/featureimage-placeholder.jpg" alt="Bild zu '.get_the_title().'"/>';
							}
						?>	
						<h4><?php the_title(); ?></h4>
					</a>
				</li>
			<?php endwhile; ?>
		</li>

		<?php else: ?>
		<p>No related photos.</p>
		<?php endif; ?>
	</div>
