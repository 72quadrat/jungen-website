<?php
/**
 * jungen functions and definitions
 *
 * @package jungen
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) )
	$content_width = 640; /* pixels */

/*
 * Load Jetpack compatibility file.
 */
require( get_template_directory() . '/inc/jetpack.php' );

if ( ! function_exists( 'jungen_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */
function jungen_setup() {

	/**
	 * Custom template tags for this theme.
	 */
	require( get_template_directory() . '/inc/template-tags.php' );

	/**
	 * Custom functions that act independently of the theme templates
	 */
	require( get_template_directory() . '/inc/extras.php' );

	/**
	 * Make theme available for translation
	 * Translations can be filed in the /languages/ directory
	 * If you're building a theme based on jungen, use a find and replace
	 * to change 'jungen' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'jungen', get_template_directory() . '/languages' );

	/**
	 * Add default posts and comments RSS feed links to head
	 */
	add_theme_support( 'automatic-feed-links' );

	/**
	 * Enable support for Post Thumbnails on posts and pages
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	/**
	 * Add Thumbnail Size
	 */
	if ( function_exists( 'add_image_size' ) ) { 
		add_image_size( 'feature-thumb', 770, 385, true ); //(cropped)
	}

	/**
	 * This theme uses wp_nav_menu() in one location.
	 */
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'jungen' ),
	) );

	/**
	 * Enable support for Post Formats
	 */
	add_theme_support( 'post-formats', array( 'video' ) );
}
endif; // jungen_setup
add_action( 'after_setup_theme', 'jungen_setup' );

/**
 * Setup the WordPress core custom background feature.
 *
 * Use add_theme_support to register support for WordPress 3.4+
 * as well as provide backward compatibility for WordPress 3.3
 * using feature detection of wp_get_theme() which was introduced
 * in WordPress 3.4.
 *
 * @todo Remove the 3.3 support when WordPress 3.6 is released.
 *
 * Hooks into the after_setup_theme action.
 */
// function jungen_register_custom_background() {
// 	$args = array(
// 		'default-color' => 'ffffff',
// 		'default-image' => '',
// 	);

// 	$args = apply_filters( 'jungen_custom_background_args', $args );

// 	if ( function_exists( 'wp_get_theme' ) ) {
// 		add_theme_support( 'custom-background', $args );
// 	} else {
// 		define( 'BACKGROUND_COLOR', $args['default-color'] );
// 		if ( ! empty( $args['default-image'] ) )
// 			define( 'BACKGROUND_IMAGE', $args['default-image'] );
// 		add_custom_background();
// 	}
// }
// add_action( 'after_setup_theme', 'jungen_register_custom_background' );

/**
 * Register widgetized area and update sidebar with default widgets
 */
function jungen_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'jungen' ),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'jungen_widgets_init' );

/**
 * Enqueue scripts and styles
 */
function jungen_scripts() {
	wp_enqueue_style( 'jungen-style', get_template_directory_uri() . '/css/basic.css' );

	//wp_enqueue_script( 'jungen-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js', array(), '20130115', true );

	// The concentaed Plugin Scripts
	wp_enqueue_script( 'plugins', get_template_directory_uri() . '/js/plugins-ck.js', array('jquery'), '20130115', true );

	// The concentaed Plugin Scripts
	wp_enqueue_script( 'scripts', get_template_directory_uri() . '/js/scripts-ck.js', array('plugins'), '20130708', true );

	// if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
	// 	wp_enqueue_script( 'comment-reply' );
	// }

	// if ( is_singular() && wp_attachment_is_image() ) {
	// 	wp_enqueue_script( 'jungen-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20120202' );
	// }

}
add_action( 'wp_enqueue_scripts', 'jungen_scripts' );

/**
 * Implement the Custom Header feature
 */
//require( get_template_directory() . '/inc/custom-header.php' );

/**
 * Customize Tiny MCE
 *
**/
// TinyMCE: First line toolbar customizations
if( !function_exists('base_extended_editor_mce_buttons') ){
	function base_extended_editor_mce_buttons($buttons) {
		// The settings are returned in this array. Customize to suite your needs.
		return array(
			'formatselect', 'bold', 'italic', 'bullist', 'numlist', 'link', 'unlink', 'blockquote', 'outdent', 'indent', 'charmap', 'removeformat', 'spellchecker', 'fullscreen', 'wp_more', 'wp_help'
		);
		/* WordPress Default
		return array(
			'bold', 'italic', 'strikethrough', 'separator', 
			'bullist', 'numlist', 'blockquote', 'separator', 
			'justifyleft', 'justifycenter', 'justifyright', 'separator', 
			'link', 'unlink', 'wp_more', 'separator', 
			'spellchecker', 'fullscreen', 'wp_adv'
		); */
	}
	add_filter("mce_buttons", "base_extended_editor_mce_buttons", 0);
}
// TinyMCE: Second line toolbar customizations
if( !function_exists('base_extended_editor_mce_buttons_2') ){
	function base_extended_editor_mce_buttons_2($buttons) {
		// The settings are returned in this array. Customize to suite your needs. An empty array is used here because I remove the second row of icons.
		return array();
		/* WordPress Default
		return array(
			'formatselect', 'underline', 'justifyfull', 'forecolor', 'separator', 
			'pastetext', 'pasteword', 'removeformat', 'separator', 
			'media', 'charmap', 'separator', 
			'outdent', 'indent', 'separator', 
			'undo', 'redo', 'wp_help'
		); */
	}
	add_filter("mce_buttons_2", "base_extended_editor_mce_buttons_2", 0);
}
// Customize the format dropdown items
if( !function_exists('base_custom_mce_format') ){
	function base_custom_mce_format($init) {
		// Add block format elements you want to show in dropdown
		$init['theme_advanced_blockformats'] = 'p,h2,h3,h4';
		// Add elements not included in standard tinyMCE dropdown p,h1,h2,h3,h4,h5,h6
		//$init['extended_valid_elements'] = 'code[*]';
		return $init;
	}
	add_filter('tiny_mce_before_init', 'base_custom_mce_format' );
}

/**
 * Add Stylesheet to the Visual Editor
 *
**/
function my_theme_add_editor_styles() {
    add_editor_style( 'css/custom-editor-style.css' );
}
add_action( 'init', 'my_theme_add_editor_styles' );

/**
 * Add CSS to Backend
 *
**/ 
function customAdminStyles() {
$url = get_bloginfo('template_url') . '/css/wp-admin.css'; // Pfad der CSS-Datei !! Bitte anpassen.
echo '<!-- custom admin css -->
<link rel="stylesheet" type="text/css" href="' . $url . '" />
<!-- /end custom adming css -->';
}
add_action('admin_head', 'customAdminStyles');
/**
 * The customized Gallery shortcode.
 *
 * This implements the functionality of the Gallery Shortcode for displaying
 * WordPress images on a post.
 *
 * @since 2.5.0
 *
 * @param array $attr Attributes of the shortcode.
 * @return string HTML content to display gallery.
 */
remove_shortcode('gallery', 'gallery_shortcode');
add_shortcode('gallery', 'gallery_shortcode_jungen');

function gallery_shortcode_jungen($attr) {
	$post = get_post();

	static $instance = 0;
	$instance++;

	if ( ! empty( $attr['ids'] ) ) {
		// 'ids' is explicitly ordered, unless you specify otherwise.
		if ( empty( $attr['orderby'] ) )
			$attr['orderby'] = 'post__in';
		$attr['include'] = $attr['ids'];
	}

	// Allow plugins/themes to override the default gallery template.
	$output = apply_filters('post_gallery', '', $attr);
	if ( $output != '' )
		return $output;

	// We're trusting author input, so let's at least make sure it looks like a valid orderby statement
	if ( isset( $attr['orderby'] ) ) {
		$attr['orderby'] = sanitize_sql_orderby( $attr['orderby'] );
		if ( !$attr['orderby'] )
			unset( $attr['orderby'] );
	}

	extract(shortcode_atts(array(
		'order'      => 'ASC',
		'orderby'    => 'menu_order ID',
		'id'         => $post->ID,
		'itemtag'    => 'div',
		'icontag'    => 'div',
		'captiontag' => 'div',
		'columns'    => 1,
		'size'       => 'large',
		'include'    => '',
		'exclude'    => ''
	), $attr));

	$id = intval($id);
	if ( 'RAND' == $order )
		$orderby = 'none';

	if ( !empty($include) ) {
		$_attachments = get_posts( array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );

		$attachments = array();
		foreach ( $_attachments as $key => $val ) {
			$attachments[$val->ID] = $_attachments[$key];
		}
	} elseif ( !empty($exclude) ) {
		$attachments = get_children( array('post_parent' => $id, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
	} else {
		$attachments = get_children( array('post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
	}

	if ( empty($attachments) )
		return '';

	if ( is_feed() ) {
		$output = "\n";
		foreach ( $attachments as $att_id => $attachment )
			$output .= wp_get_attachment_link($att_id, $size, true) . "\n";
		return $output;
	}

	$itemtag = tag_escape($itemtag);
	$captiontag = tag_escape($captiontag);
	$icontag = tag_escape($icontag);
	$valid_tags = wp_kses_allowed_html( 'post' );
	if ( ! isset( $valid_tags[ $itemtag ] ) )
		$itemtag = 'div';
	if ( ! isset( $valid_tags[ $captiontag ] ) )
		$captiontag = 'div';
	if ( ! isset( $valid_tags[ $icontag ] ) )
		$icontag = 'div';

	$columns = intval($columns);
	$itemwidth = $columns > 0 ? floor(100/$columns) : 100;
	$float = is_rtl() ? 'right' : 'left';

	$selector = "carousel{$instance}";

	$gallery_style = $gallery_div = '';
	if ( apply_filters( 'use_default_gallery_style', true ) )
		$gallery_style = "";
	$size_class = sanitize_html_class( $size );
	$gallery_div = "<div id='$selector' class='carousel slide'>";
	$output = apply_filters( 'gallery_style', $gallery_style . "\n\t\t" . $gallery_div );
	$output .= '<div class="carousel-inner">';

	$i = 0;
	$imagecount = 0;
	foreach ( $attachments as $id => $attachment ) {
		$imagecount++;
		$link = isset($attr['link']) && 'file' == $attr['link'] ? wp_get_attachment_link($id, $size, false, false) : wp_get_attachment_link($id, $size, true, false);
		$image = wp_get_attachment_image($id, $size);

		if ($imagecount == 1) {
			$output .= "<{$itemtag} class='item active'>";
		} else {
			$output .= "<{$itemtag} class='item'>";
		}
		$output .= $image;
		if ( $captiontag && trim($attachment->post_excerpt) ) {
			$output .= "
				<{$captiontag} class='carousel-caption'>
				<h4>" . wptexturize($attachment->post_title) . "</h4>
				<p>" . wptexturize($attachment->post_excerpt) . "</p>
				</{$captiontag}>";
		}
		$output .= "</{$itemtag}>";
		if ( $columns > 0 && ++$i % $columns == 0 )
			$output .= '';
	}

	$output .= "</div><!-- close inner -->";
	$output .= "<a class='carousel-control left' href='#".$selector."' data-slide='prev'>&lsaquo;</a>";
	$output .= "<a class='carousel-control right' href='#".$selector."' data-slide='next'>&rsaquo;</a>";
	$output .= "</div><!-- close carousel -->\n";

	return $output;
}
// Collapse Shortcode
function collapse_shortcode( $atts, $contents = null ) {
	$contents = str_replace('<br />', '', $contents);
	$contents = explode("####", $contents);
	$html = '<div class="accordion" id="accordion">'."\n";
	foreach ($contents as $key => $content) {
		if ($key % 2 == 0) {
			$next = $key+1;
			$html .= '  <div class="accordion-group">'."\n";
			$html .= '    <div class="accordion-heading">';
			$html .= '      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse'.$next.'"><strong>'.strip_tags($content).'</strong></a>'."\n";
			$html .= '    </div>'."\n";
		} else {
			$html .= '    <div id="collapse'.$key.'" class="accordion-body collapse">'."\n";
			$html .= '      <div class="accordion-inner">'.$content.'</div>'."\n";
			$html .= '    </div>'."\n";
			$html .= '  </div>'."\n";
		}
	}
	$html .= '</div>'."\n";
	return $html;
}
add_shortcode( 'collapse', 'collapse_shortcode' );

// Get Users by Post date
// function jungen_get_users_ordered_by_post_date($args = '') {
//     // Prepare arguments
//     if (is_string($args) && '' !== $args)
//         parse_str($args, $args);
//     $asc = (isset($args['order']) && 'ASC' === strtoupper($args['order']));
//     //unset($args['orderby']);
//     //unset($args['order']);


//     // Get ALL users
//     $users = get_users($args);
//     $post_dates = array();
//     if ($users) {
//         // For EACH user ...
//         foreach ($users as $user) {
//             $ID = $user->ID;

//             // ... get ALL posts (per default sorted by post_date), ...
//             $posts = get_posts('author='.$ID);
//             $post_dates[$ID] = '';

//             // ... then use only the first (= newest) post
//             if ($posts) $post_dates[$ID] = $posts[0]->post_date;
//         }
//     }

//     // Sort dates (according to order), ...
//     if (! $asc)
//         arsort($post_dates);

//     // ... then set up user array
//     $users = array();
//     foreach ($post_dates as $key => $value) {
//         // $user = get_userdata($key);
//         // $users[] = $user->ID;
//         $users[] = get_userdata($key);
//     }
//     return $users;
// }

// Get random Users for frontpage friends part
function jungen_get_users_random($role = ''){
	$args = array(
		'blog_id'      => $GLOBALS['blog_id'],
		'role'         => $role,
		'meta_key'     => '',
		'meta_value'   => '',
		'meta_compare' => '',
		'meta_query'   => array(),
		'include'      => array(),
		'exclude'      => array(),
		'orderby'      => 'login',
		'order'        => 'ASC',
		'offset'       => '',
		'search'       => '',
		'number'       => '',
		'count_total'  => false,
		'fields'       => 'all',
		'who'          => ''
	 );
	$users = get_users($args);
	shuffle($users);
	return $users;
}

function jungen_get_social(){
	$facebook = json_decode(file_get_contents('https://graph.facebook.com/1381148072116871/posts?access_token=351733854912656|Ef0x2U7EoELfqa7EKDC0AN4Q4GI'));
	$facebook = $facebook->data;
	$facebookhtml = '';
	if (is_array($facebook)) {
		$i = 0;
		$facebookhtml .= '<aside id="facebook" class="widget">';
		$facebookhtml .= '<h1 class="widget-title"><a href="https://www.facebook.com/meintestgelaende" target="_blank" title="Mein Testgelände auf Facebook">Facebook</a></h1>';
		$facebookhtml .= '<ul>';
		foreach ($facebook as $key => $post) {		
			if ($post->created_time AND $post->message) {
				$i++;
				if ($i < 5) {
					$link = 'https://www.facebook.com/'.str_replace('_', '/posts/', $post->id);
					$time = '<strong>'.date('j.m.Y', strtotime($post->created_time)).'</strong>';
					$shorttext = wordwrap($post->message, 140, "\0");
					$shorttext = preg_replace('/^(.*?)\0(.*)$/is', '$1', $shorttext);

					$facebookhtml .= '<li>';
					$facebookhtml .= $time.': '.$shorttext.' ...';
					$facebookhtml .= '<a href="'.$link.'" target="_blank" title="Beitrag auf Facebook anschauen"> Mehr</a> ';
					$facebookhtml .= '</li>';
				}
			}
		}
		$facebookhtml .= '</ul>';
		$facebookhtml .= '</aside>';
	}
	echo $facebookhtml;
}