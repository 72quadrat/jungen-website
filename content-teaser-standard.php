<?php
/**
 * Displays Teasers on frontpage and all archive pages
 *
 * @package jungen
 */
?>

<div class="row">
	<div class="span3">
		<?php 
			if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.

				// $img_id = get_post_thumbnail_id($post->ID); // This gets just the ID of the img
				// $attr = array(
				// 	'alt' => get_post_meta($img_id , '_wp_attachment_image_alt', true)
				// );

				echo '<div class="feature-thumb">';
				the_post_thumbnail('feature-thumb', $attr);
				echo '</div>';
			} else {
				echo '<img src="'.get_template_directory_uri().'/img/featureimage-placeholder.jpg" alt="Bild zu '.get_the_title().'"/>';
			}
		?>		
	</div>
	<div class="span6">
		<header class="entry-header">

			<h1 class="entry-title bigtext"><a href="<?php the_permalink(); ?>" rel="bookmark" class="entry-title-link" title="Zum Artikel '<?php the_title(); ?>'"><?php the_title(); ?></a></h1>	

		</header><!-- .entry-header -->
		<footer class="entry-meta">
			<ul class="inline">
				<?php jungen_posted_on(); ?>

				<!--
				<?php if ( 'post' == get_post_type() ) : ?>
					<li><i class="icon-time"></i> <?php the_time('j. F Y'); ?></li>
				<?php endif; ?>

				<li><i class="icon-user"></i> <?php the_author_posts_link(); ?></li>

				<?php if ( 'post' == get_post_type() ) : // Hide category and tag text for pages on Search ?>
					<?php
						/* translators: used between list items, there is a space after the comma */
						$categories_list = get_the_category_list( __( ', ', 'jungen' ) );
						if ( $categories_list && jungen_categorized_blog() ) :
					?>
						<li class="cat-links">
							<i class="icon-folder-open"></i> <?php the_category(', '); ?>
						</li>
					<?php endif; // End if categories ?>

					<?php
						/* translators: used between list items, there is a space after the comma */
						$tags_list = get_the_tag_list( '', __( ', ', 'jungen' ) );
						if ( $tags_list ) :
					?>
						<li class="tags-links">
							<i class="icon-tags"></i> <?php the_tags(); ?>
						</li>
					<?php endif; // End if $tags_list ?>
				<?php endif; // End if 'post' == get_post_type() ?>

				<?php if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) : ?>
				<span class="comments-link"><i class="icon-comment-alt"></i> <?php comments_popup_link( __( 'Leave a comment', 'jungen' ), __( '1 Comment', 'jungen' ), __( '% Comments', 'jungen' ) ); ?></span><br>
				<?php endif; ?>

				<?php edit_post_link( __( 'Edit', 'jungen' ), '<span class="edit-link"><i class="icon-edit"></i> ', '</span>' ); ?>
				-->
			</ul>
		</footer><!-- .entry-meta /span2 -->

		<div class="entry-summary">
			<?php the_excerpt(); ?>
			<a href="<?php the_permalink(); ?>" rel="bookmark" class="btn" title="Zum Artikel '<?php the_title(); ?>'">Beitrag ansehen</a>
		</div><!-- .entry-summary -->
	</div>

</div><!-- /row -->