<?php
/**
 * The template for displaying search forms in jungen
 *
 * @package jungen
 */
?>
	<form method="get" id="searchform" class="searchform input-append" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
		<label for="s" class="screen-reader-text visuallyhidden"><?php _ex( 'Search', 'assistive text', 'jungen' ); ?></label>
		<input type="search" class="field" name="s" value="<?php echo esc_attr( get_search_query() ); ?>" id="s" placeholder="<?php echo esc_attr_x( 'Suche &hellip;', 'placeholder', 'jungen' ); ?>" />
		<!-- <input type="submit" class="submit btn" id="searchsubmit" value="<?php echo esc_attr_x( 'Suchen', 'submit button', 'jungen' ); ?>" /> -->
		<button class="btn" type="submit"><i class="icon-search"></i><span class="visuallyhidden">Suchen</span></button>

	</form>
