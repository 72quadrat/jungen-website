Jungen Website
==============

Die Website nutzt das ["_s" Theme von Automattic](https://github.com/Automattic/_s/)
und das CSS-Framework [Twitter Bootstrap](http://twitter.github.io/bootstrap/)
letzteres jedoch in der [SCSS Variante](https://github.com/jlong/sass-twitter-bootstrap) da als Preprocessor SASS eingesetzt wird.
Es wurde jedoch in [_type.scss](https://github.com/jlong/sass-twitter-bootstrap/blob/master/lib/_type.scss)
einiges bei den Zeilnabständen verändert da dies negative Auswirkungen auf die großen Überschriften gehabt hätte.

## Plugins
Es werden möglichst wenige Plugins eingesetzt um die Seite relativ schlank zu halten.

### Front-End
* **Maintenance Mode** Zum temporären Abschalten der Seite bei Updates oder anderen grundlegenden Änderungen.
[link](http://sw-guide.de/wordpress/plugins/maintenance-mode/)
* **Advanced Custom Fields** Für Spezialeingabefelder wie z.B. den Bildnachweis
[link](http://www.advancedcustomfields.com/)
* **Wordpress Ad Widget** Als einfacher Weg um zwischenzeitlich Banner einbinden zu können – wird derzeit dauerhaft eingesetzt.
[link](https://wordpress.org/plugins/ad-widget/)
* **2 Click Social Media Buttons** Für die Social Bookmark-Funktion
[link](http://ppfeufer.de/wordpress-plugin/2-click-social-media-buttons/)
* **Contact Form 7** Für das Mitmachformular
[link](http://contactform7.com/)
* **Simple Local Avatars** Um die Autorenbidler auch ohne gravatar.com zu realisieren
[link](http://10up.com/plugins/simple-local-avatars-wordpress/)
* **WP-Polls** Für die Votings
[link](http://lesterchan.net/portfolio/programming/php/)

### Backend
* **Better WP Security** Für mehr Sicherheit. Setzt auch z.B. das Kontaktformular auf SSL-Zugang. Inzwischen umgenannt in iThemes Security.
[link](https://wordpress.org/plugins/better-wp-security/)
* **Antispam Bee** Eine datenschutzkonforme Möglichkeit Kommentar-Spam zu minimieren.
[link](http://antispambee.de/)
* **WP Super Cache** Zur Optimierung der Performance der Seite. Man könnte die Einstellungen noch etwas verbessern oder auf W3 Total Cache umziehen.
[link](http://ocaoimh.ie/wp-super-cache/)
* **WP-Piwik** Für den Piwik Seitenzähler
[link](http://wordpress.org/extend/plugins/wp-piwik/)
* **Co-Authors Plus** Macht es möglich mehrer Autoren einem Beitrag zuzuweisen und auch die Rechte entsprechend zu vergeben. Leider sind die Logiken dahinter etwas kurios aber es wird offensichtlich benötigt.
[link](https://wordpress.org/plugins/co-authors-plus/)
* **WP Comments Moderators** So kann jeder User Kommentare freigeben und moderieren
[link](https://wordpress.org/plugins/fay-comments-moderators/)

## Überschriften
Für die Typografie der Headlines wurde die kostenlose [Alpha Echo](http://www.fontsquirrel.com/fonts/Alpha-Echo) eingesetzt.
Für den engen Satz wurden die Umlaute Ä,Ö und Ü jedoch manuell bearbeitet und eine Schriftanpassung vorgenommen.  
Um die Headlines auf die Breite zu zwingen wurde das jQuery Plugin [jSlabify](https://github.com/gschoppe/jSlabify) eingesetzt. Dies funktioniert vollautomatisch und kann vom Redakteur nicht gesteuert werden.

### Webfont Loader
Der Webfont wird mittels [Typekit Webfontloader](https://github.com/typekit/webfontloader) geladen. Dies ist notwendigt damit das "jSablify-Plugin" erst ausgeführt wird nachdem die Schrift fertigt geladen und gerendert wurde.

## Specials
* Der Visual Editor ist angepasst worden um unnötige Buttons zu entfernen und z.B. die Stilvorlagen sichtbarer zu machen
* Der Visual Editor hat ein einfaches kleines CSS bekommen
* Das Admin-CSS ist ebenfalls angepasst: Im Media-Uploader ist der Bereich ausgeblendet worden wo auswählbar ist, wie das Bild ausgerichtet werden soll, wie groß das Bild sein soll, und ob ein Link gesetzt werden soll.

## Author Page
Die Autoren-Seiten auf denen die Beiträge eines Autors gelistet werden sind etwas angepasst worden. Zu erreichen z.B. über einen klick auf ein Profilbild auf der Startseite.

Es werden nur Autoren abgebildet die mind. einen Beitrag geschrieben haben. Admins werden nicht angezeigt – Es werden nur Benutzer mit der Rolle "Author" ausgegeben.

Die Inhalte des Headers werden über die Profilseite eines Autors eingegeben.
Für den kleinen Text werden die biographischen Angaben genutzt. Um ein Video anzuzgeigen muß der Link zu dem Video in dem Feld "Jabber /Google Talk" eingegeben werden.

## Formular Mitmachen
Auf der Startseiten-Bühne ist ein Reiter "Mitmachen". Der Textinhalt hier ist im Theme eingegeben und kann nicht über das CMS angepasst werden.
Das Formular ist über das Plugin "Contact Form 7" eingerichtet worden. Derzeit wird die E-Mail noch an hoener@neueshandeln.de geschickt


## Entwicklungsumgebung

##### Preprocessor
Für die CSS-Programmierung wurde Preprocessing eingsetzt. Genauergesagt wird SASS eingesetzt. Als Tool wurde hier massiv [Codekit](http://incident57.com/codekit/) eingesetzt. Hierüber wurden auch die Scripts kombiniert.

##### Frameworks und Plugins
Es wird das alte Twitter Bootstrap eingesetzt – im SCSS Fork.
Das Theme setzt auf HTML5 Boilerplate auf.

##### Javascript Plugins
* modernizr - für's HTML5
* bootstrap-tab.js - Für die Autoren-Reiter auf der Startseite
* bootstrap-tooltip.js - Für's Impressum
* bootstrap-carousel.js - Für Galerien
* bootstrap-transition.js - benötigt für die Animation der Reiter
* bootstrap-collapse.js - für die Netiquette
* jquery.jSlabify.js - siehe oben für die Headline
* jquery.fitvids.js - für responsive Video-Einbettungen

## ToDo
* Auf Bootstrap 3 updaten und Bestandteile verschlanken
* Related Posts mit nem flotteren Plugin als YARP versuchen?
* Browser-Caching und GZip wieder aktivieren
* Bilder kleiner reinrechnen und mit Imsanity Plugin übergroße Bilder vom Server räumen
* Plugin-CSS mit Haupt-CSS kombinieren oder nur auf benötigen Seiten laden
* Mobil Benutzernamen kleiner schreiben – die sind oft zu lang.
