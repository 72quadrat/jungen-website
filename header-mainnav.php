<?php
/**
 * Just the main navigation
 * The meta nav is done with wordpress menus - name: "Metanavigation"
 * The main nav shows the available categories and a hand coded "ALle" Navigation for
 * the frontpage
 *
 * @package jungen
 */
?>
<div id="mainnav">
	<div class="container">

		<div class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#">Menu</a> <i class="icon-chevron-down"></i>
			<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
				<?php if (is_home()) : ?>
					<li class="current-cat"><a href="#content">Alles</a></li>
				<?php else : ?>
					<li><a href="<?php bloginfo('wpurl'); ?>">Alles</a></li>
				<?php endif; ?>
				<?php 
					// Exclude first Category because it's the standard cat
					wp_list_categories( array('depth'=>1, 'title_li'=>'', 'exclude'=>1) ); 
				?>
				<li class="divider"></li>
				<?php wp_nav_menu( array('menu' => 'Metanavigation', 'items_wrap' => '%3$s', 'container' => false )); ?>
			</ul>
		</div>

		<div class="horizontal">
			<ul>
				<?php if (is_home()) : ?>
					<li class="current-cat">Alles</li>
				<?php else : ?>
					<li><a href="<?php bloginfo('wpurl'); ?>">Alles</a></li>
				<?php endif; ?>
				<?php 
					// Exclude first Category because it's the standard cat
					wp_list_categories( array('depth'=>1, 'title_li'=>'', 'exclude'=>1) ); 
				?>
			</ul>
		</div>
	</div>
</div>