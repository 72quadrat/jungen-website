
// Slabtext Function
function slabtext() {
	if (jQuery('.single').length > 0) {
		jQuery("h1.bigtext").jSlabify({
			viewportBreakpoint: 767,
			targetFont        : false,
			fontZoom          : 1,
			minCharsPerLine   : 8,
			slabRatio         : 3,
			minWordSpace      : 0
		});
	} else {
		jQuery("h1.bigtext").jSlabify({
			viewportBreakpoint: 767,
			targetFont        : false,
			fontZoom          : 1,
			minCharsPerLine   : 1,
			slabRatio         : 7,
			minWordSpace      : 0,
		});
	};
}


//jQuery Stuffs
jQuery(document).ready(function(){
	//console.log('doc ready');

	// Slabtext
	slabtext();
	setTimeout("slabtext()", 500);
	setTimeout("slabtext()", 1500);

	// Fit Video
	jQuery("article").fitVids();

	// Tooltip Bootsrap
	jQuery('#stage a, .avatar-wrap a').tooltip({
		placement: 'top'
	});

	// jQuery Carousel
	jQuery('.carousel').carousel({
		interval: false
	});

	// Tweak HTML
	jQuery('#submit').addClass('btn btn-primary');

	// Hide and show friends
	jQuery('#stage2 .btn-circle').click(function(){
		jQuery('#stage2 li.friend-hidden').removeClass('friend-hidden');
		jQuery(this).parent().hide();
		jQuery('#stage2 .showless').show();
	});
	jQuery('#stage2 .showless a').click(function(){
		jQuery('#stage2 li').not('.friend').not('.circle').addClass('friend-hidden');
		jQuery('#stage2 li.circle').show();
		jQuery(this).parent().hide();
	});

	jQuery('#stage1 .btn-circle').click(function(){
		jQuery('#stage1 li.friend-hidden').removeClass('friend-hidden');
		jQuery(this).parent().hide();
		jQuery('#stage1 .showless').show();
	});
	jQuery('#stage1 .showless a').click(function(){
		jQuery('#stage1 li').not('.friend').not('.circle').addClass('friend-hidden');
		jQuery('#stage1 li.circle').show();
		jQuery(this).parent().hide();
	});

 });