<?php
/*
Template Name: Sitemap
*/

get_header(); ?>

	<div id="primary" class="content-area span9">
		<div id="content" class="site-content" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'page' ); ?>

				<?php wp_nav_menu( array('menu' => 'Sitemap', 'menu_class'=>'nav nav-tabs nav-stacked' )); ?>

			<?php endwhile; // end of the loop. ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
